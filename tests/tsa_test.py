import shutil
import os
import sys
sys.path.append('../')
from targeted_sequencing_analytics_suite import tsa
from unittest import TestCase
import pandas as pd
from matplotlib.testing.decorators import image_comparison
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import colors
import matplotlib.gridspec as gridspec
import scipy.spatial.distance as distance
import scipy.cluster.hierarchy as sch
import numpy as np
import pybedtools
import seaborn as sns


def ListFiles(sPath, extension):
    # returns a list of names (with extension, with full path) of all files
    # in folder sPath
    lsFiles = []
    lsLabels = []
    for sName in os.listdir(sPath):
        if os.path.isfile(os.path.join(sPath, sName)) and sName.endswith(extension):
            lsFiles.append(os.path.join(sPath, sName))
            fileName, fileExtension = os.path.splitext(sName)
            sName = os.path.basename(fileName).split('.')[0]
            lsLabels.append(sName)
    return lsFiles, lsLabels


class tsaTest(TestCase):

    def setUp(self):
        self.list_of_bam_files, self.labels = ListFiles(
            "test_data/bam/", ".sorted.bam")
        self.binomial_result_file_list, self.status_labels = ListFiles(
            "test_data/variant_status", ".tsv")
        self.region_file = "test_data/targets/AA4DD.bed"
        os.mkdir("test_data/output")
        self.outdir = "test_data/output"
        self.expected = "test_data/expected"

    def tearDown(self):
        shutil.rmtree("test_data/output")

    def test_get_coverage(self):
        result = tsa(
            list_of_bam_files=self.list_of_bam_files,
            binomial_result_file_list=self.binomial_result_file_list,
            region_file=self.region_file,
            outdir=self.outdir,
            labels=self.labels)

    def test_number_of_bam_files(self):
        self.assertEqual(len(self.list_of_bam_files), 3)

    def test_number_of_variant_status(self):
        self.assertEqual(len(self.binomial_result_file_list), 3)

    def test_length_zygosity_array(self):
        tmp = pd.read_csv(self.binomial_result_file_list[0], sep='\t')
        self.assertEqual(tmp.shape[0], 48)
        self.assertEqual(tmp.shape[1], 17)
        self.assertEqual(len(tmp['zygosity']), 48)

    def test_wrong_length_zygosity_array(self):
        tmp = pd.read_csv(self.binomial_result_file_list[0], sep='\t')
        self.assertNotEqual(tmp.shape[0], 30)
        self.assertNotEqual(tmp.shape[1], 30)
        self.assertNotEqual(len(tmp['zygosity']), 25)

    def test_length_variant_frequency_array(self):
        tmp = pd.read_csv(self.binomial_result_file_list[0], sep='\t')
        self.assertEqual(tmp.shape[0], 48)
        self.assertEqual(tmp.shape[1], 17)
        self.assertEqual(len(tmp['var_freq']), 48)

    def test_wrong_length_variant_frequency_array(self):
        tmp = pd.read_csv(self.binomial_result_file_list[0], sep='\t')
        self.assertNotEqual(tmp.shape[0], 30)
        self.assertNotEqual(tmp.shape[1], 30)
        self.assertNotEqual(len(tmp['var_freq']), 148)

    def test_path_to_alignments(self):
        assert os.path.exists("test_data/bam")

    def test_path_to_variant_status(self):
        assert os.path.exists("test_data/variant_status")

    #@image_comparison(baseline_images=['coverage_histogram_in_amplicon_region', 'cumulative_coverage_saturation_plot', 'targetd_region_coverage'])
    def test_plot_coverage_histogram(self):

        self.tsa = tsa(list_of_bam_files=self.list_of_bam_files, binomial_result_file_list=self.binomial_result_file_list, region_file=self.region_file, outdir=self.outdir, labels=self.labels)

        N = len(self.list_of_bam_files)
        sample_colors = cm.get_cmap('Paired', N)
        palette = sample_colors(np.arange(N))
        # samples = []
        depth_list = []
        percent_list = []
        fraction_list = []
        # Set the initial layout
        fig1 = plt.figure()
        fig2 = plt.figure()
        fig3 = plt.figure()
        # fig.set_size_inches(18.5, 14.5)
        # I decided to split the figure into 3 different ones
        ax1 = fig1.add_subplot(1, 1, 1)
        ax1.set_title("Coverage histogram in amplicon region")
        ax1.set_xlabel('Coverage')
        # ax1.set_xlim((min(depth), max(depth)))
        ax2 = fig2.add_subplot(1, 1, 1)
        ax2.set_title("Saturation curve : Cumulative detailed histogram")
        ax2.set_xlabel('Coverage')
        # ax2.set_xlim((min(depth), max(depth)))
        ax3 = fig3.add_subplot(1, 1, 1)
        ax3.set_title('Target Region Coverage')
        ax3.set_xlabel('Coverage(X)')
        ax3.set_ylabel(
            'Fraction of capture target bases ' + u"\u2267" + ' depth')
        ax3.set_ylim(0.0, 1.0)
        # and populate the plots
        for bam_file in self.list_of_bam_files:
            sample_id = os.path.splitext(os.path.basename(bam_file))[0]
            # samples.append(sample_id)
            # print sample_id
            # collect needed arrays
            coverage_hist, all_histogram = self.tsa.get_coverage_histogram(
                bam_file, os.path.join(self.outdir, sample_id + ".coverage.hist.txt"))
            all_histogram = all_histogram[all_histogram.depth.astype(int) != 0]
            depth = np.asarray(all_histogram['depth'], dtype=int)
            depth_list.append(depth)
            percent = np.asarray(all_histogram['percent'], dtype=float)
            percent_list.append(percent)
            fraction = 1 - np.cumsum(percent, dtype=float)
            fraction_list.append(fraction)
        # Figure 1

        ax1.hist(depth_list, bins=500, color=palette,
                 histtype='stepfilled', stacked=True)
        ax1.set_xlim((min(depth_list[0]), max(depth_list[0])))
        ax1.legend(self.labels, loc='best')
        # Figure 2
        ax2.hist(depth_list, cumulative=True, bins=500,
                 histtype='stepfilled', color=palette, stacked=True)
        ax2.set_xlim((min(depth_list[0]), max(depth_list[0])))
        ax2.legend(self.labels, loc='best')
        # Figure 3
        for dep, frac in zip(depth_list, fraction_list):
            ax3.plot(dep, frac)
        ax3.legend(self.labels, loc='best')
        # fig.show()
        fig1.tight_layout()
        fig1.savefig(
            os.path.join(self.expected, 'coverage_histogram_in_amplicon_region.pdf'))
        fig2.tight_layout()
        fig2.savefig(
            os.path.join(self.expected, 'cumulative_coverage_saturation_plot.pdf'))
        fig3.tight_layout()
        fig3.savefig(os.path.join(self.expected, 'targetd_region_coverage.pdf'))

    def test_plot_coverage_heatmap(self):
        region = pybedtools.BedTool(self.region_file)
        result = region.multi_bam_coverage(bams=self.list_of_bam_files, output=os.path.join(
            self.outdir, "multicoverage.hist.txt"))
        coverage_df = pd.read_table(result.fn, header=None)
        ncols = coverage_df.shape[1]
        data = coverage_df[list(coverage_df.columns[3:ncols])].astype(int)
        # Set columns
        data.columns = self.labels
        # Set index
        data_index = [str(chrom) + ":" + str(start) + "--" + str(end) for chrom, start, end in zip(list(coverage_df[
            coverage_df.columns[0]]), list(coverage_df[coverage_df.columns[1]]), list(coverage_df[coverage_df.columns[2]]))]
        data['coordinates'] = data_index
        data = data.set_index('coordinates')
        # plot
        fig = plt.figure()
        sns.heatmap(data, square=True)
        plt.xticks(rotation=90, fontsize=5)
        plt.yticks(fontsize=5)
        plt.title("Coverage within amplicon regions")
        plt.ylabel("amplicon regions")
        plt.xlabel("samples")
        fig.tight_layout()
        fig.savefig(os.path.join(self.expected, "test_plot_coverage_heatmap.pdf"))

    def test_plot_allelic_frequencies_heatmap(self):
        labels = []
        all_samples_allele_frequencies = pd.DataFrame()
        for sample_file in self.binomial_result_file_list:
            # Create the sample name out of the file path
            sample_name = os.path.basename(sample_file).split(
                '.tsv')[0].replace('-', '_')
            labels.append(sample_name)
            # Load the variant_status table into a temporary dataframe
            df_tmp = pd.read_csv(sample_file, sep='\t')
            # Extract the var_freq column
            var_freq = df_tmp['var_freq'].values
            # Add it to the general dataframe that will be plotted
            all_samples_allele_frequencies[
                sample_name] = var_freq.astype(float)
        fig = plt.figure()
        plt.subplot(1, 1, 1)
        plt.title('Allele Frequencies')
        plt.xlabel('Samples')
        plt.ylabel('Positions')
        Y = all_samples_allele_frequencies
        plt.xticks(np.arange(Y.shape[1]))
        plt.xticks(rotation=90, fontsize=8)
        # Optional, I am sortng the positions in the dataframe to split those with high allele freq
        # and low allel freq
        plt.gca().set_xticklabels(self.labels)
        palette = plt.cm.RdYlBu
        palette.set_bad('black', 1.0)
        plt.grid(False)
        Ysorted = Y.sort(labels, ascending=[True] * len(self.labels))
        plt.imshow(Ysorted, cmap=palette, vmin=0, vmax=1,
                   interpolation='nearest', aspect='auto')
        plt.colorbar()
        fig.tight_layout()
        fig.savefig(os.path.join(self.expected, "allele_freq.pdf"))

    def test_plot_allelic_frequencies_heatmap_with_clusters(self):

        self.tsa = tsa(list_of_bam_files=self.list_of_bam_files, binomial_result_file_list=self.binomial_result_file_list, region_file=self.region_file, outdir=self.outdir, labels=self.labels)

        labels = []
        all_samples_allele_frequencies = pd.DataFrame()
        for sample_file in self.tsa.binomial_result_file_list:
                # Create the sample name out of the file path
            sample_name = os.path.basename(sample_file).split(
                '.tsv')[0].replace('-', '_')
            labels.append(sample_name)
            # Load the variant_status table into a temporary dataframe
            df_tmp = pd.read_csv(sample_file, sep='\t')
            # Extract the var_freq column
            var_freq = df_tmp['var_freq'].values
            # Add it to the general dataframe that will be plotted
            all_samples_allele_frequencies[
                sample_name] = var_freq.astype(float)
        tmp = pd.read_csv(self.tsa.binomial_result_file_list[0], sep='\t')
        index_col = [str(a) + "--" + str(b)
                     for a, b in zip(tmp.chrom, tmp.coord)]
        all_samples_allele_frequencies.index = index_col
        # drop nan values
        all_samples_allele_frequencies = all_samples_allele_frequencies.fillna(
            80)
        # set the threshold for NaN
        threshold = 1.1
        cmap = cm.Purples
        cmap.set_over('slategray')
        all_samples_allele_frequencies.reset_index(drop=True)
        # calculate pairwise distances for rows
        pairwise_dists = distance.squareform(
            distance.pdist(all_samples_allele_frequencies))
        # create clusters
        clusters = sch.linkage(pairwise_dists, method='complete')
        # make dendrograms black rather than letting scipy color them
        sch.set_link_color_palette(['black'])
        # dendrogram without plot
        # den = sch.dendrogram(clusters, color_threshold = np.inf, no_plot = True)
        row_clusters = clusters
        # calculate pairwise distances for columns
        col_pairwise_dists = distance.squareform(
            distance.pdist(all_samples_allele_frequencies.T))
        # cluster
        col_clusters = sch.linkage(col_pairwise_dists, method='complete')
        # heatmap with row names
        fig = plt.figure(figsize=(15, 20), dpi=10)
        heatmapGS = gridspec.GridSpec(
            2, 2, wspace=0.0, hspace=0.0, width_ratios=[0.25, 1], height_ratios=[0.25, 1])
        ### col dendrogram ###
        col_denAX = fig.add_subplot(heatmapGS[0, 1])
        col_denD = sch.dendrogram(col_clusters, color_threshold=np.inf)
        self.tsa.clean_axis(col_denAX)
        ### row dendrogram ###
        row_denAX = fig.add_subplot(heatmapGS[1, 0])
        row_denD = sch.dendrogram(
            row_clusters, color_threshold=np.inf, orientation='right')
        self.tsa.clean_axis(row_denAX)
        # all_samples_allele_frequencies.index = [ 'position ' + str(x) for x in all_samples_allele_frequencies.index ]
        # all_samples_allele_frequencies.columns = all_samples_allele_frequencies.columns[col_denD['leaves']]
        ### heatmap ####
        heatmapAX = fig.add_subplot(heatmapGS[1, 1])
        axi = heatmapAX.imshow(all_samples_allele_frequencies.ix[row_denD['leaves'], col_denD[
                               'leaves']], interpolation='nearest', aspect='auto', origin='lower', cmap=cm.Purples, vmax=threshold)
        heatmapAX.grid(False)
        self.tsa.clean_axis(heatmapAX)
        ## row labels ##
        heatmapAX.set_yticks(
            np.arange(all_samples_allele_frequencies.shape[0]))
        heatmapAX.yaxis.set_ticks_position('right')
        heatmapAX.set_yticklabels(
            all_samples_allele_frequencies.index[row_denD['leaves']], fontsize=5)
        ## col labels ##
        heatmapAX.set_xticks(
            np.arange(all_samples_allele_frequencies.shape[1]))
        xlabelsL = heatmapAX.set_xticklabels(
            all_samples_allele_frequencies.columns[col_denD['leaves']])
        # rotate labels 90 degrees
        for label in xlabelsL:
            label.set_rotation(90)
        # remove the tick lines
        for l in heatmapAX.get_xticklines() + heatmapAX.get_yticklines():
            l.set_markersize(0)
        ### scale colorbar ###
        scale_cbGSSS = gridspec.GridSpecFromSubplotSpec(
            1, 2, subplot_spec=heatmapGS[0, 0], wspace=0.0, hspace=0.0)
        # colorbar for scale in upper left corner
        scale_cbAX = fig.add_subplot(scale_cbGSSS[0, 1])
        # note that we could pass the norm explicitly with norm=my_norm
        cb = fig.colorbar(axi, scale_cbAX)
        cb.set_label('Allele Frequencies')
        # move ticks to left side of colorbar to avoid problems with
        # tight_layout
        cb.ax.yaxis.set_ticks_position('left')
        # move label to left side of colorbar to avoid problems with
        # tight_layout
        cb.ax.yaxis.set_label_position('left')
        cb.outline.set_linewidth(0)
        # make colorbar labels smaller
        tickL = cb.ax.yaxis.get_ticklabels()
        for t in tickL:
            t.set_fontsize(t.get_fontsize() - 3)
        fig.tight_layout()
        fig.savefig(os.path.join(self.expected, "allele_freq_clustered.pdf"))

    def test_plot_zygosity_matrix(self, cluster=0):

        self.tsa = tsa(list_of_bam_files=self.list_of_bam_files, binomial_result_file_list=self.binomial_result_file_list, region_file=self.region_file, outdir=self.outdir, labels=self.labels)

        labels = []
        all_samples_zygosity = pd.DataFrame()
        for sample_file in self.tsa.binomial_result_file_list:
            # Create the sample name out of the file path
            sample_name = os.path.basename(sample_file).split(
                '.tsv')[0].replace('-', '_')
            labels.append(sample_name)
            # Load the variant_status table into a temporary dataframe
            df_tmp = pd.read_csv(sample_file, sep='\t')
            # convert zygosity into states
            remap_zygosity = {
                'homozygote_wildtype': -1, 'heterozygote': 0, 'homozygote_mutant': 1, 'unknown': 80}
            df_tmp = df_tmp.replace({'zygosity': remap_zygosity})
            # Extract the zygosity column
            zigosity = df_tmp['zygosity'].values
            # Add it to the general dataframe that will be plotted
            all_samples_zygosity[sample_name] = zigosity.astype(float)
        #all_samples_zygosity = all_samples_zygosity[tmporder_a9255]
        tmp = pd.read_csv(self.tsa.binomial_result_file_list[0], sep='\t')
        #f = lambda x: 2324 if str(x).lower() in ['x', 'y'] else x
        #index_col = [float(str(f(a)) + "." + str(b)) for a, b in zip(tmp.chrom, tmp.coord)]
        #all_samples_zygosity.index = index_col
        # drop nan values
        all_samples_zygosity = all_samples_zygosity.sort()
        all_samples_zygosity = all_samples_zygosity.fillna(80)
        # set the threshold for NaN
        threshold = 1.1
        cmap = colors.ListedColormap(
            ['#CC3333', '#FFCC33', '#0066CC'], 'indexed')
        # bounds = [-1, -0.9, 0.9, 1]
        # norm = colors.BoundaryNorm(bounds, cmap.N)
        # cmap = cm.Purples
        cmap.set_over('slategray')
        all_samples_zygosity.reset_index(drop=True)
        if cluster == 0:
            # calculate pairwise distances for rows
            pairwise_dists = distance.squareform(
                distance.pdist(all_samples_zygosity))
            # create clusters
            clusters = sch.linkage(pairwise_dists, method='complete')
            # make dendrograms black rather than letting scipy color them
            sch.set_link_color_palette(['black'])
            # dendrogram without plot
            # den = sch.dendrogram(clusters, color_threshold = np.inf, no_plot = True)
            row_clusters = clusters
            # calculate pairwise distances for columns
            col_pairwise_dists = distance.squareform(
                distance.pdist(all_samples_zygosity.T))
            # cluster
            col_clusters = sch.linkage(col_pairwise_dists, method='complete')
            # heatmap with row names
            fig = plt.figure(figsize=(15, 20), dpi=10)
            heatmapGS = gridspec.GridSpec(
                2, 2, wspace=0.0, hspace=0.0, width_ratios=[0.25, 1], height_ratios=[0.25, 1])
            ### col dendrogram ###
            col_denAX = fig.add_subplot(heatmapGS[0, 1])
            col_denD = sch.dendrogram(col_clusters, color_threshold=np.inf)
            self.tsa.clean_axis(col_denAX)
            ### row dendrogram ###
            row_denAX = fig.add_subplot(heatmapGS[1, 0])
            row_denD = sch.dendrogram(
                row_clusters, color_threshold=np.inf, orientation='right')
            self.tsa.clean_axis(row_denAX)
            # all_samples_zygosity.index = [ 'position ' + str(x) for x in all_samples_zygosity.index ]
            # all_samples_zygosity.columns = all_samples_zygosity.columns[col_denD['leaves']]
            ### heatmap ####
            heatmapAX = fig.add_subplot(heatmapGS[1, 1])
            axi = heatmapAX.imshow(all_samples_zygosity.ix[row_denD['leaves'], col_denD['leaves']],
                                   interpolation='nearest', aspect='auto', origin='lower', cmap=cmap, vmax=threshold)
            heatmapAX.grid(False)
            self.tsa.clean_axis(heatmapAX)
            ## row labels ##
            heatmapAX.set_yticks(np.arange(all_samples_zygosity.shape[0]))
            heatmapAX.yaxis.set_ticks_position('right')
            heatmapAX.set_yticklabels(
                all_samples_zygosity.index[row_denD['leaves']], fontsize=5)
            ## col labels ##
            heatmapAX.set_xticks(np.arange(all_samples_zygosity.shape[1]))
            xlabelsL = heatmapAX.set_xticklabels(
                all_samples_zygosity.columns[col_denD['leaves']])
            # rotate labels 90 degrees
            for label in xlabelsL:
                label.set_rotation(90)
            # remove the tick lines
            for l in heatmapAX.get_xticklines() + heatmapAX.get_yticklines():
                l.set_markersize(0)
            ### scale colorbar ###
            scale_cbGSSS = gridspec.GridSpecFromSubplotSpec(
                1, 2, subplot_spec=heatmapGS[0, 0], wspace=0.0, hspace=0.0)
            # colorbar for scale in upper left corner
            scale_cbAX = fig.add_subplot(scale_cbGSSS[0, 1])
            # note that we could pass the norm explicitly with norm=my_norm
            cb = fig.colorbar(axi, scale_cbAX)
            cb.set_label('Zygosity')
            # move ticks to left side of colorbar to avoid problems with
            # tight_layout
            cb.ax.yaxis.set_ticks_position('left')
            # move label to left side of colorbar to avoid problems with
            # tight_layout
            cb.ax.yaxis.set_label_position('left')
            cb.outline.set_linewidth(0)
            # make colorbar labels smaller
            tickL = cb.ax.yaxis.set_ticklabels(
                ['', 'homozygote_wildtype', '', '', 'heterozygote', '', '', 'homozygote_mutant'])
            for t in tickL:
                t.set_fontsize(t.get_fontsize() - 3)
            fig.tight_layout()
            fig.savefig(os.path.join(self.expected, "zygosity_matrix.pdf"))
        else:
            fig = plt.figure(figsize=(10, 15), dpi=10)
            # heatmapGS = gridspec.GridSpec(2, 2, wspace=0.0, hspace=0.0, width_ratios=[0.25, 1], height_ratios=[0.25, 1])
            heatmapAX = fig.add_subplot(1, 1, 1)
            axi = heatmapAX.imshow(all_samples_zygosity, interpolation='nearest',
                                   aspect='auto', origin='lower', cmap=cmap, vmax=threshold)
            heatmapAX.grid(False)
            self.clean_axis(heatmapAX)
            ## row labels ##
            heatmapAX.set_yticks(np.arange(all_samples_zygosity.shape[0]))
            heatmapAX.yaxis.set_ticks_position('right')
            heatmapAX.set_yticklabels(all_samples_zygosity.index, fontsize=5)
            ## col labels ##
            heatmapAX.set_xticks(np.arange(all_samples_zygosity.shape[1]))
            xlabelsL = heatmapAX.set_xticklabels(all_samples_zygosity.columns)
            # rotate labels 90 degrees
            for label in xlabelsL:
                label.set_rotation(90)
            # remove the tick lines
            for l in heatmapAX.get_xticklines() + heatmapAX.get_yticklines():
                l.set_markersize(0)
            fig.tight_layout()
            fig.savefig(os.path.join(self.expected, "zygosity_matrix.pdf"))

    def test_plot_mapping_qualities_in_regions(self):
        pass

    def test_plot_in_target_outof_target_alignments(self):
        pass

    def test_plot_in_off_target_fractions(self):
        pass

    def test_plot_af_per_zygosity(self):
        pass

    def test_plot_zygosity_distribution(self):
        pass

    def test_plot_coverage_violin(self):
        pass

    def test_plot_insert_distribution(self):
        pass
