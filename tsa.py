import argparse
import os
from targeted_sequencing_analytics_suite import tsa


parser = argparse.ArgumentParser(description='Process different bam files and output plots on coverage and enrichment')

parser.add_argument('--path_to_bams',
                    help='''Path to bam files, sorted and indexed.''')
parser.add_argument('--targets',
                    help='''File containing amplicon regions in Bed format''')
parser.add_argument('--variant_status_path',
                    help='''Path to binomial test result files (tsv files)''')
parser.add_argument('--bam_extension',
                    help='''Extension of the alignments''')
parser.add_argument('--outdir',
                    help='''directory to save all different plots''')
parser.add_argument('--custom_order', required=False,
                    help='''order of the samples we want to display''' )

args = parser.parse_args()


def read_custom_order(order_file):
    """
    Read a custom order that the user provides and output a list that will
    be given to some functions
    The file should contain one sample name per line
    """
    samples_order = []
    with open(order_file, 'r') as f:
      samples_order = [line.strip() for line in f]

    return samples_order


def ListFiles(sPath, extension):
    # returns a list of names (with extension, with full path) of all files
    # in folder sPath
    lsFiles = []
    lsLabels = []
    for sName in os.listdir(sPath):
        if os.path.isfile(os.path.join(sPath, sName)) and sName.endswith(extension):
            lsFiles.append(os.path.join(sPath, sName))
            fileName, fileExtension = os.path.splitext(sName)
            sName = os.path.basename(fileName).split('.')[0]
            lsLabels.append(sName)
    return lsFiles, lsLabels


def main():

    list_of_bam_files, labels = ListFiles(args.path_to_bams, args.bam_extension)
    list_of_variant_status, status_labels = ListFiles(args.variant_status_path, ".tsv")

    result = tsa(list_of_bam_files=list_of_bam_files,
                 binomial_result_file_list=list_of_variant_status,
                 region_file=args.targets,
                 outdir=args.outdir,
                 labels=labels)

    result.warning()
    #result.plot_coverage_histogram()
    #result.plot_in_target_outof_target_alignments("in_target_out_of_target.pdf")
    result.plot_in_off_target_fractions("in_target_out_of_target_fractions.pdf")
    result.plot_coverage_heatmap("coverage_heatmap_in_amplicons.pdf")
    #result.plot_allelic_frequencies_heatmap("allelic_frequencies_heatmap.pdf")
    result.plot_allelic_frequencies_heatmap_with_clusters("allelic_frequencies_heatmap_clustered.pdf")
    # Check if the user provided a custom order
    if(args.custom_order):
        result.plot_zygosity_matrix("zygosity_matrix_clustered.pdf", cluster=1, custom_order=read_custom_order(args.custom_order))
    else:
        result.plot_zygosity_matrix("zygosity_matrix_clustered.pdf", cluster=0)

    #result.plot_mapping_qualities_in_regions("mappinq_qualities_in_region.pdf")
    result.plot_af_per_zygosity("allelic_frequencies_per_zygosity")
    # Check if the user provided a custom order
    if(args.custom_order):
        result.plot_coverage_violin("coverage_violin_distributions.pdf", custom_order=read_custom_order(args.custom_order))
    else:
        result.plot_coverage_violin("coverage_violin_distributions.pdf")

if __name__ == '__main__':
    main()
